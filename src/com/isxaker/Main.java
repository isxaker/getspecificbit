package com.isxaker;

public class Main {

    public static void main(String[] args) {
        int num1 = 65535; //  1111 1111 1111 1111
        int num2 = 65536; //1 0000 0000 0000 0000
        int num3 = 10; // 1010

        boolean bit_10_FromNum1_1 = getSpecificBit1(num1, 10);
        boolean bit_10_FromNum1_2 = getSpecificBit2(num1, 10);
        boolean bit_10_FromNum1_3 = getSpecificBit3(num1, 10);
        boolean bit_10_FromNum1_4 = getSpecificBit4(num1, 10);

        boolean bit_10_FromNum2_1 = getSpecificBit1(num2, 10);
        boolean bit_10_FromNum2_2 = getSpecificBit2(num2, 10);
        boolean bit_10_FromNum2_3 = getSpecificBit3(num2, 10);
        boolean bit_10_FromNum2_4 = getSpecificBit4(num2, 10);

        boolean bit_2_FromNum3_1 = getSpecificBit1(num3, 2);
        boolean bit_2_FromNum3_2 = getSpecificBit2(num3, 2);
        boolean bit_2_FromNum3_3 = getSpecificBit3(num3, 2);
        boolean bit_2_FromNum3_4 = getSpecificBit4(num3, 2);
    }

    public static boolean getSpecificBit1(int num, int bitNum) {
        return ((num >> bitNum) & 1) != 0;
    }

    public static boolean getSpecificBit2(int num, int bitNum) {
        return (num & (1 << bitNum - 1)) != 0;
    }

    public static boolean getSpecificBit3(int num, int bitNum) {
        return (num & (1 << bitNum)) != 0;
    }

    public static boolean getSpecificBit4(int num, int bitNum) {
        return ((num >> bitNum) & 1) > 0;
    }

    public static int changeLestZeroBit(int num) {
        return num | (num + 1);
    }
}
